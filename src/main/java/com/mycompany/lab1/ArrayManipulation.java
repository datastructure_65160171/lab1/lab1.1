/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author brzho
 */
public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        System.out.println("Numbers array:");
        traverseArrayUsingForLoop(numbers); //3.Print the elements of the "numbers" array using a for loop.
        System.out.println("\nNames array:");
        traverseArrayUsingEnhancedForLoop(names); //4.Print the elements of the "names" array using a for-each loop.
        
        //5.Initialize the elements of the "values" array
        values[0] = 0.8;
        values[1] = 9.5;
        values[2] = 7.4;
        values[3] = 6.5;
        
        //6.Calculate and print the sum of all elements in the "numbers" array
        int sum = 0;
        for (int i = 0;i<numbers.length;i++){
            sum = sum + numbers[i];
        }
        System.out.println("\nSum of numbers: " + sum);
        //for (int num : numbers) {
          //  sum += num;
        //}
        //System.out.println("\nSum of numbers: " + sum);
        
        //7.Find and print the maximum value in the "values" array.
         double maxValue = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > maxValue) {
                maxValue = values[i];
            }
        }
        System.out.println("Maximum value in values array: " + maxValue);
        
        //8.Create a new string array named "reversedNames" with the same length as the "names" array.
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[names.length - 1 - i] = names[i];
        }
        System.out.println("Reversed names array:");
        for (String name : reversedNames) {
            System.out.print(name + " ");
        }
        System.out.println();
        
        
        for (int i = 0; i < numbers.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[j] < numbers[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = numbers[i];
            numbers[i] = numbers[minIndex];
            numbers[minIndex] = temp;
        }
        
        System.out.println("Sorted numbers array:");
        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
    
    
    //For 3.
    public static void traverseArrayUsingForLoop(int[] arr){
        for (int i = 0; i< arr.length; i++){
            System.out.print(arr[i] + " ");
        }
    }
    //For 4.
    public static void traverseArrayUsingEnhancedForLoop(String[] arr){
        for (String A : arr){
            System.out.print(A + " ");
        }
    }
    
}




